# 👋 Welcome to Programming for GIS! 👋

In this module you will get acquainted with the building blocks needed for creating (semi-)automated geospatial analysis pipelines with Python. You will start with raw data and end with a repeatable analysis and datasets containing results.

You will learn a number of skills that will help you become a self-reliant and effective geo-IT expert who is able to collaborate with fellow developers on well-documented applications. You will learn how to

- Perform basic data cleaning, transformation and spatial analysis with the Python programming language.
- Install Python and use several functionalities of an integrated development environment. 
- Design and setup a repeatable analysis workflow with Python.
- Use built-in and third-party modules/packages and create their own functions within Python. 
- Quickly and effectively find and evaluate external Python modules for their fitness of purpose for the job at hand. 
- Read online documentation and write their own documentation.  
- Use the basic functionality of public/private collaboration platforms such as GitLab/GitHub, StackOverflow, GeoForum and others.

Programming and web development is best learned by doing. This module therefore consists of (fairly) short theoretical lessons followed by extensive hands-on and guided exercises.

## Goal and scope

This module aims to teach you all of the practical skills needed to create basic analysis pipelines. In doing so it favors completeness over depth: instead of focusing on a single topic we strive to shine a light on the whole workflow/toolchain with the goal of enabling you to independently create functioning and repeatable analysis from scratch.

This module does not, therefore, aim to teach advanced geospatial analysis and modelling.

## Two study tracks: standard and advanced

This module is split in two study tracks to cater for the differences in the students' prior programming/development knowledge and experience. The tutors will advise students which track suits them best on the basis of a questionnaire.

![](studyplan.png)

### Standard track

The `Standard` track is meant for students who are engaging with development for the first time. This track focuses on teaching the basics and omits some advanced topics that do not bring immediate value to beginning developers. Students in this track are allowed to skip chapters and sections marked as `Advanced`.

Please refer to the [Standard study plan](#) for the study pace.

### Advanced track

The `Advanced` track is meant for students who have some programming/development experience. In this track students will learn about version control using git and more advanced principles of the Python programming language. Students following the `Advanced` track should complete the whole module.

Please refer to the [Advanced study plan](#) for the study pace.

# Module outline, chapter sections and assignments

This module consists of 8 chapters

1. \[Advanced\] [Working with git](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/1-working-with-git): introduction to the basics of software version control and online collaboration platforms
2. [Python programming 1 - the basics](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/2-python-programming-1): gentle introduction to the Python programming language and debugging
3. [Python programming 2 - scripts and functions](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/3-python-programming-2): learn how to use Python files, and create your own functions
4. [Preparing data with Python](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/4-preparing-data-with-python): introduction to data cleaning in Python
5. [Documentation, packages and StackOverflow](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/5-documentation-packages-and-stackoverflow): learn how to use Python modules, and look for and consume documentation
6. \[Advanced\] [Python programming 3 - decorators and classes](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/6-python-programming-3): learn about more advanced mechanisms of the python programming language
7. [Spatial analysis with Python](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/7-spatial-analysis-with-python): introduction to spatial analysis in Python using the Shapely module
8. [Geospatial on the web](https://gitlab.com/unigis/programming-for-gis/tree/main/lecture-notes/8-geospatial-on-the-web): learn about web mapping and generate an app with a few lines of code

## Chapter sections

Each chapter consist of two sections: theory and (guided) exercises.

### Theory

The `Resources` section of each chapter contains the learning material. This can be an interactive tutorial, chapters from a book, or it will be written notes.

It is advised to briefly look at the exercises section before diving into the resources (to know what to lookout for). But before you complete the exercises from the `Exercises` section, study all the materials in the `Resources` section.

### Exercises

Most chapters are accompanied by a practical assignment or a step-by-step guide that gives you the opportunity to apply the freshly gained knowledge. Exercises are not graded, but some must be completed by uploading deliverables to canvas. These deliverables are described in the assignments folder.

## Assignment: election results analysis

To pass this module students are required to complete two graded assignments.

The student is asked to analyze the results of an election based on Dutch open data.

In the first graded assignment the student is expected to locate and obtain the required data, transform and clean it as needed for the envisioned spatial analysis.

Deliverables: a functioning data processing pipeline, its source code and a short reflection on the taken path that discusses the strengths/weaknesses of the data cleaning pipeline.

The second graded assignment involves the spatial analysis of the acquired data from the first assignment.

Deliverables: a functioning geospatial analysis pipeline, its source code and a short reflection on the taken path that discusses the strengths/weaknesses of the analysis pipeline.

There are two versions of the assignment (Standard and Advanced) to cater for the differences in the student's programming knowledge and experience, see each [assignment's definition](https://gitlab.com/unigis/programming-for-gis/tree/main/assignments/) for more information.

### Method of Assessment

The course contains five assignments. Three formative assignments and two summative assignments. All assignments are mandatory and have a specific deadline. Deadlines are strictly upheld. All assignments must be completed in time and the graded ones should receive a passing grade (55 or above) in order to pass the course. Assignments submitted late are not graded. For Assignments that are not submitted, submitted late or that score below 55, there is a resit opportunity during the summer recess, with a grade cap at 60. Assignments that score 50-54 can be resat within one month following the end of the course, with a grade cap at 60. In case you have any questions about the assignment deadlines, please contact the Programme Coordinator, Sanne Hettinga, [s.hettinga@vu.nl](mailto:s.hettinga@vu.nl).

The first and third assignments are formative, meaning that you will receive feedback but will not be graded. They are meant to maximise your learning experience and help to prepare you for the second and fourth assignments, which are graded. The final assignment is meant to broaden your horizon and is graded either fail or pass; with the option to occasionally award “excellent” when clearly above expectations. The graded assignments all need to be passed with a score of 55 or above (or ‘pass’ in the case of TAA5).

# Learning resources, tools and instruments

In this module we will use the following resources:

## The Coder's Apprentice
["The Coder's Apprentice"](https://www.spronck.net/pythonbook/) is a course book that is aimed at teaching Python 3 to students who are completely new to programming. Contrary to many of the other books that teach Python programming, this book assumes no previous knowledge of programming on the part of the students, and contains numerous exercises that allow students to train their programming skills.

The Coder's Apprentice is available in English and Dutch.

## LearnPython
Get started learning Python with this [interactive Python tutorial](https://www.learnpython.org/).
Whether you are an experienced programmer or not, this website is intended for everyone who wishes to learn the Python programming language.

## RealPython
At [Real Python](https://realpython.com/) you can learn all things Python from the ground up. Everything from the absolute basics of Python, to web development and web scraping, to data visualization, and beyond.
## Learngitbranching
Solve real problems and enhance your skills with browser based hands on labs without any downloads or configuration. Within this course ([Learn Git](https://learngitbranching.js.org/)) the ``Introduction Sequence & Ramping Up`` from the main section and ``Push & Pull -- Git Remotes!`` from the remote section are used (Advanced track). More advanced examples are also available.
## GitLab
GitLab (this site) is a collaboration platform for software development. You will use it to consume the lecture notes and communicate with the tutors and fellow students through the [issue tracker](https://gitlab.com/unigis/programming-for-gis/issues). Advanced students will learn a lot more about GitLab and version control in general in Chapter 1.

# Getting help: questions, inquiries, feedback

The primary communication channel for this module is the [issue tracker](https://gitlab.com/unigis/programming-for-gis/issues) located on the top of this page under `Issues`.

Use the issue tracker to ask questions about the lecture materials, web/software development in general, to report bugs and typos and suggest improvements to the lecture notes and/or code examples.

Please note that your fellow students will see your questions and vice versa. We encourage you to check the issue tracker frequently and answer/engage with the posted questions.

Issues, questions and suggestions that have been answered, solved or implemented are _closed_. You can find them in the [Closed](https://gitlab.com/unigis/programming-for-gis/issues?scope=all&utf8=%E2%9C%93&state=closed) section of the issue tracker.

**Private questions**

[Standard] Questions not fit for the public, e.g. questions about exercises and assignments that require you to reveal too much of your code, can be send via email to [Peter](mailto:peter@peterschols.com).

[Advanced] In Chapter 1 you will create a GitLab project that is only visible to you and the tutors. You can use its issue tracker to ask questions that are not fit for the public one e.g. questions about exercises and assignments that require you to reveal too much of your code. Don't forget to add user PeterSchols as a member to you private repository. 

# Getting started

Ready to dive into the lecture notes and exercises? Perfect!👍 

You will find them in the `lecture notes` folder at the top of this page.

Students following the Standard track should start with Chapter 2: 
Python programming 1 - the basics.

Students in the Advanced track should start with Chapter 1: Working with git.
