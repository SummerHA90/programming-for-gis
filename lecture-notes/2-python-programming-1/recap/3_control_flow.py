# -*- coding: utf-8 -*-

# Conditionals and control flow

print('''
---
--- Basic control flow ---
---
''')

country = 'NL'

if country == 'ES':
    greeting = 'Buena noches!'
else:
    greeting = 'Good evening!'

print(greeting)


print('''
---
--- More than one condition ---
---
''')

country = 'SE'

if country == "NL":
    greeting = 'Goede avond!'
elif country == "ES":
    greeting = 'Buena noches!'
elif country == "SE":
    greeting = 'God kväl'
else:
    greeting = 'Good evening!'

print(greeting)


print('''
---
--- Advanced control flow ---
---
''')

continent = 'Europe'
country = 'NL'

# You can nest if statemens to create
# complex logic flows

if continent == 'Europe':
    if country == 'NL':
        greeting = 'Welcome to {} in {}.'.format(country, continent)
    else:
        greeting = 'Welcome to {}'.format(continent)
else:
    greeting = 'Welcome to the world!'

print(greeting)

# The logical operators and/or allow you to check several conditions at once

if continent == "Europe" and country == "NL":
    print(country, continent)
else:
    print("You are not in NL, Europe.")


print('''
---
--- Truthful expressions ---
---
''')
# if statements evaluate the "truthfulness" of expressions
# most of Python's constructs, expressions and statements
# have a boolean (i.e. True/False) representation. For
# example, empty collections ('', (), [], {}) evaluate
# to False as does None.

countries = ['NL', 'SE', 'ES']
country = 'US'

if country in countries:
    continent = 'You are in Europe!'
else:
    continent = 'Your continent is unknown.'

print(continent)


countries = []

if countries:
    print('True')
else:
    print('False')