# Chapter 3. Python programming 2 - scripts and functions

In this module you will learn how to utilze python scripts (self created modules) and write your own functions.

## Rationale

Modules refer to a file containing Python statements and definitions.
A file containing Python code, for example: example.py, is called a module, and its module name would be example.
We use modules to break down large programs into small manageable and organized files. Furthermore, modules provide reusability of code.
We can define our most used functions in a module and import it, instead of copying their definitions into different programs.

## Outcomes

- basic proficiency in Python
  - writing functions
  - using (your own scripts as) modules

## Resources

### Course


| Course type | Course                                                                  | Relevant chapters                         | Remarks                                                                                                                             |
| ----------- | ----------------------------------------------------------------------- | ---------------------------------------------: | ------------------------------------------------------------------------------------------------------------ |
| Website     | [Learn Python](https://www.learnpython.org/)                            |learn the basics: "Functions" and "Modules and Packages" | Best suited for students who are just starting with programming and prefer a step-by-step interactive course |
| Book        | [The Coder's Apprentice](http://www.spronck.net/pythonbook/index.xhtml) | 8, 15 & 27      | Best suited for students with no prior knowledge who prefer text-based study (available in english and dutch).                                |


### Reference

- [The official Python tutorial](https://docs.python.org/3/tutorial/index.html) on python.org

### Lecture notes

In the remainder of this chapter we will create example module and look at some characteristics of a module. 

### Creating a module

In the resources you have learned about modules. The cool thing about modules written in Python is that they are exceedingly straightforward to build. All you need to do is create a file that contains legitimate Python code and then give the file a name with a .py extension. That’s it! No special syntax or voodoo is necessary.

For example, suppose you have created a file called mod.py containing the following:

```python
crs = "EPSG:28992"
transforms = ["EPSG:4326", "EPSG:3857"]

def foo(arg):
    print(f'arg = {arg}')
```
Several objects are defined in mod.py:

- crs (a string)
- transforms (a list)
- foo() (a function)

Assuming mod.py is in an appropriate location, which you will learn more about shortly, these objects can be accessed by importing the module as follows:

```python
>>> import mod
>>> print(mod.crs)
EPSG:28992
>>> mod.transforms
["EPSG:4326", "EPSG:3857"]
>>> mod.foo(transforms)
arg = ["EPSG:4326", "EPSG:3857"]
```
### The Module Search Path

Continuing with the above example, let’s take a look at what happens when Python executes the statement:

```python
import mod
```

When the interpreter executes the above import statement, it searches for mod.py in a list of directories assembled from the following sources:

- The directory from which the input script was run or the current directory if the interpreter is being run interactively
- The list of directories contained in the PYTHONPATH environment variable, if it is set. (The format for PYTHONPATH is OS-dependent but should mimic the PATH environment variable.)
- An installation-dependent list of directories configured at the time Python is installed
The resulting search path is accessible in the Python variable sys.path, which is obtained from a module named sys:

```python
>>> import sys
>>> sys.path
```
`Note: The exact contents of sys.path are installation-dependent.`

Thus, to ensure your module is found, you need to do one of the following:

- Put mod.py in the directory where the input script is located or the current directory, if interactive
- Modify the PYTHONPATH environment variable to contain the directory where mod.py is located before starting the interpreter
    - Or: Put mod.py in one of the directories already contained in the PYTHONPATH variable
- Put mod.py in one of the installation-dependent directories, which you may or may not have write-access to, depending on the OS

There is actually one additional option: you can put the module file in any directory of your choice and then modify sys.path at run-time so that it contains that directory. For example, in this case, you could put mod.py in directory C:\Users\peter and then issue the following statements:

```python
>>> sys.path.append(r'C:\Users\peter')
>>> import mod
```

Once a module has been imported, you can determine the location where it was found with the module’s __file__ attribute:

```python
>>> import mod
>>> mod.__file__
```

The directory portion of __file__ should be one of the directories in sys.path.

### The import Statement
Module contents are made available to the caller with the import statement. The import statement takes many different forms, shown below.

```import <module_name>```

The simplest form is the one already shown above:

```python
import <module_name>
```

Note that this does not make the module contents directly accessible to the caller. Each module has its own private symbol table, which serves as the global symbol table for all objects defined in the module. Thus, a module creates a separate namespace, as already noted.

The statement import <module_name> only places <module_name> in the caller’s symbol table. The objects that are defined in the module remain in the module’s private symbol table.

From the caller, objects in the module are only accessible when prefixed with <module_name> via dot notation, as illustrated below.

After the following import statement, mod is placed into the local symbol table. Thus, mod has meaning in the caller’s local context:

```python
>>> import mod
>>> mod
<module 'mod' from 'path/on/your/system/mod.py'>
```
But crs and foo remain in the module’s private symbol table and are not meaningful in the local context:

```python
>>> crs
NameError: name 's' is not defined
>>> foo('quux')
NameError: name 'foo' is not defined
```

To be accessed in the local context, names of objects defined in the module must be prefixed by mod:
```python
>>> mod.crs
'EPSG:28992'
>>> mod.foo('hello')
arg = hello
```

Several comma-separated modules may be specified in a single import statement:
```python
import <module_name>[, <module_name> ...]
```

```from <module_name> import <name(s)>```

An alternate form of the import statement allows individual objects from the module to be imported directly into the caller’s symbol table:
```python
from <module_name> import <name(s)>
```

Following execution of the above statement, <name(s)> can be referenced in the caller’s environment without the <module_name> prefix:

```python
>>> from mod import s, foo
>>> crs
'EPSG:28992'
>>> foo('hello')
arg = hello
```

Because this form of import places the object names directly into the caller’s symbol table, any objects that already exist with the same name will be overwritten.

It is even possible to indiscriminately import everything from a module at one fell swoop:

```python
from <module_name> import *
```

This will place the names of all objects from <module_name> into the local symbol table, with the exception of any that begin with the underscore (_) character.

For example:
```python
>>> from mod import *
>>> crs
'EPSG:28992'
>>> transforms
["EPSG:4326", "EPSG:3857"]
>>> foo
<function foo at 0x03B449C0>
```

This isn’t necessarily recommended in large-scale production code. It’s a bit dangerous because you are entering names into the local symbol table en masse. Unless you know them all well and can be confident there won’t be a conflict, you have a decent chance of overwriting an existing name inadvertently. However, this syntax is quite handy when you are just mucking around with the interactive interpreter, for testing or discovery purposes, because it quickly gives you access to everything a module has to offer without a lot of typing.

```from <module_name> import <name> as <alt_name>```

It is also possible to import individual objects but enter them into the local symbol table with alternate names:

```python
from <module_name> import <name> as <alt_name>[, <name> as <alt_name> …]
```

This makes it possible to place names directly into the local symbol table but avoid conflicts with previously existing names:
```python
>>> crs = 'EPSG:3042'

>>> from mod import crs as ref
>>> crs
'EPSG:3042'
>>> ref
'EPSG:28992'
```

```import <module_name> as <alt_name>```

You can also import an entire module under an alternate name:
```python
import <module_name> as <alt_name>
```

## Exercise

Execute each script in the `recap/` folder and study what the code does as a:

1.  refresher from the Resources
2.  way to discover new functions and features

Try to create a module based on the code from chapter 2.
