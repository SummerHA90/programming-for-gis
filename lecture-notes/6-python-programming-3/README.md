# Chapter 6. Decorators and classes
Decorators are a very powerful and useful tool in Python since it allows programmers to modify the behaviour of function or class. Decorators allow us to wrap another function in order to extend the behaviour of the wrapped function, without permanently modifying it.

Object-oriented programming (OOP) on the other hand refers to a type of computer programming (software design) in which programmers define the data type of a data structure, and also the types of operations (functions) that can be applied to the data structure.

In this way, the data structure becomes an object that includes both data and functions. In addition, programmers can create relationships between one object and another. For example, objects can inherit characteristics from other objects.

If you are new to object-oriented programming languages, you will need to know a few basics before you can get started with code. The following Webopedia definitions will help you better understand object-oriented programming:

* ```Abstraction```: The process of picking out (abstracting) common features of objects and procedures.
* ```Class```: A category of objects. The class defines all the common properties of the different objects that belong to it.
* ```Inheritance```: a feature that represents the "is a" relationship between different classes.
* ```Object```: a self-contained entity that consists of both data and procedures to manipulate the data.
* ```Polymorphism```: A programming language's ability to process objects differently depending on their data type or class.

## Outcomes
- basic knowledge of the subjects within object oriented programming
- basic proficiency in creating and managing decorators and objects

## Resources

| Course type | Course                                                            | Remarks                |
| ----------- | ----------------------------------------------------------------- | ---------------------- |
| Website | [Object-Oriented Programming (OOP) in Python 3](https://realpython.com/python3-object-oriented-programming/) | Website with in-depth tutorials for all levels of Python users.     |

In this course you will learn how to:
- Create decorators
- Define a Class in Python
- Instantiate an Object in Python
- Inherit From Other Classes in Python

### Lecture notes

#### Simple decoration

![](img/simpledeco.png)

The so-called decoration happens at the following line:
```say_whee = my_decorator(say_whee)```

To put simply: __decorators wraps a function, modifying its behavior.__

#### Decorator syntax

![](img/syntax.PNG)

In the example above, the trace function is a decorator. That means that it takes a single argument (which is normally the function being decorated). Internally, it defines a function traced() that prints out a line of text, calls the decorated function with whatever arguments it was called with itself, prints out another line of text and then returns the result obtained from the decorated function. Then, trace returns the function it has just defined.

This means that you can apply trace() to any function, and the result will do just what the original function did as well as
printing out a line before and after the call to the decorated function. This is how most decorators work (although as always there are some smart people who have found non-standard ways to use decorators that were not originally intended by the specification). That's why you often see the internal function written to accept any combination of positional and keyword arguments—it means that the decorator can be applied to any function, no matter what its signature.

Remember, the decorator syntax is really just an abbreviation; it doesn't do anything that you couldn't do without the syntax. When you write ```@trace``` before the definition for myfunc(), it's exactly equivalent to writing ```myfunc = trace(myfunc)``` after the function definition. The syntax was added because with lo nger function definitions it was often difficult to notice the reassignment to the name when it followed the function definition. The feature was restricted to functions when it was originally introduced, but now you can also decorate classes. While this is a little bit more
complicated than decorating functions, it does have its uses.

Because the above decorator defines a function that contains a call to the decorated function as a part of its code (traced() in the example above), we say that the decorator wraps the decorated function. This has certain unfortunate side effects: mostly, the name of the function appears to change to the name of the wrapper function from inside the
decorator, and the docstring is that of the wrapper.

#### Functools wraps
Fortunately, these issues (the renaming of the decorated function and the docstring adaptation) can be handled using the wraps decorator from the functools library. This is provided precisely to ensure that decorated functions continue to "look like themselves." Until you get the hang of using it, however, it seems a little weird because it means you end up using a decorator on the wrapper function inside your
decorator! But honestly, it isn't difficult.

![](img/wraps.PNG)

#### Usecases for decorators
* logging
* timing
* debugging
* authenticating

##### Example of logging decorator
![](img/logging.png)

##### Example of timing decorator
![](img/timing.png)

##### Example of debugging decorator
![](img/debugging.png)

#### Classes
Classes model the behavior of objects in the "real" world. Methods implement the behaviors of these types of objects. Member variables hold (current) state. Classes enable us to implement new data types in Python.

The class: statement is used to define a class. The class: statement creates a class object and binds it to a name. To define a new style class (recommended), inherit from object or from another class that does.

![](img/class_a.png)![](img/class_b.png)

#### Methods
A method is a function defined in class scope and with first parameter self:

![](img/method.png)

A method as we describe it here is more properly called an instance method, in order to distinguish it from class methods and static methods.

##### The constructor
The constructor is a method named ```__init__```

![](img/constructor.png)

Note:
The self variable is explicit. It references the current object, that is the object whose method is currently executing.

#### Member variables
Defining member variables Member variables are created with assignment.

![](img/membervariables.png)

#### Class variables
Also called static data.
A class variable is shared by instances of the class.
Define at class level with assignment.
Reference with classname.variable.
Caution: self.variable = x creates a new member variable.

![](img/classvariables.png)

#### Calling methods
Use the instance and the dot operator.
Calling a method defined in the same class or a superclass:
From outside the class Use the instance:
```location.show()``` 
From within the same class Use self:
```self.a_method()```
Calling a method defined in a specific superclass Use the class (name).

##### ```__repr__``` and ```__str__```
We will look into two important python object functions that are very helpful in debugging python code by logging useful information regarding the object.

A good rule of thumb is: ```__repr__``` is for developers, ```__str__``` is for customers.

##### ```__str__()```
This method returns the string representation of the object. This method is called when print() or str() function is invoked on an object.

This method must return the String object. If we don’t implement ```__str__()``` function for a class, then built-in object implementation is used that actually calls ```__repr__()``` function.

##### ```__repr__()```
Python ```__repr__()``` function returns the object representation. It could be any valid python expression such as tuple, dictionary, string etc.

This method is called when repr() function is invoked on the object, in that case, ```__repr__()``` function must return a String otherwise error will be thrown.

#### Inheritance
Referencing superclasses Use the built-in super or the explicit name of the superclass. Use of super is preferred. For example:

![](img/inheritance_a.png)

The use of super() may solve problems searching for the base class when using multiple inheritance. A better solution is to not use multiple inheritance.
You can also use multiple inheritance. But, it can cause confusion. For example, in the following, class F inherits from both E and D:

![](img/inheritance_b.png)

##### issubclass() & isinstance()
Python issubclass() function is used to check if a class is a subclass of another class or not. Python issubclass() function syntax is:
```python
issubclass(class, classinfo)
```
This function returns True if class is a subclass of classinfo.

A class is considered a subclass of itself. We can also pass a tuple of classes as the classinfo argument, in that case, the function will return True if class is a subclass of any of the classes in the tuple.

Since the object is the base class in Python, the function will return True if classinfo is passed as object class.

Example:
![](img/subclass.png)

##### Python issubclass() vs isinstance()
Python issubclass() and isinstance() functions are very similar, except that former works with classes whereas later works with instance of classes.

#### Overriding methods
Overriding is the property of a class to change the implementation of a method provided by one of its base classes.

Overriding is a very important part of OOP since it makes inheritance utilize its full power. By using method overriding a class may "copy" another class, avoiding duplicated code, and at the same time enhance or customize part of it. Method overriding is thus a part of the inheritance mechanism.

In Python method overriding occurs by simply defining in the child class  a method with the same name of a method in the parent class. When you  define a method in the object you make this latter able to satisfy that  method call, so the implementations of its ancestors do not come in  play.

![](img/override.PNG)

Now Child objects behave differently

![](img/override_out.PNG)

#### Polymorphism
The literal meaning of polymorphism is the condition of occurrence in different forms.

Polymorphism is a very important concept in programming. It refers to the use of a single type entity (method, operator or object) to represent different types in different scenarios.

##### Polymorphism in addition operator
We know that the + operator is used extensively in Python programs. But, it does not have a single usage.

For integer data types, + operator is used to perform arithmetic addition operation. Similarly, for string data types, + operator is used to perform concatenation.

#### Function Polymorphism
There are some functions in Python which are compatible to run with multiple data types.

One such function is the len() function. It can run with many data types in Python.
```python
print(len('hello world'))
print(len(['a', 'b', 'c']))
print(len(('1', '2', '3')))
print(len({"Name": "John", "Address": "Middle of nowhere"}))
```

#### Polymorphism and Inheritance
Like in other programming languages, the child classes in Python also inherit methods and attributes from the parent class. We can redefine certain methods and attributes specifically to fit the child class, which is known as Method Overriding.

Polymorphism allows us to access these overridden methods and attributes that have the same name as the parent class.

Use inheritance on the Shape class and call in the __init__ of Square and circle the super().__init__(). (i.e. ```super().__init__("Circle")```).
Use method overriding on the area method. Square = ```self.length**2```, Circle = ```pi*self.radius**2``` and Shape = ```pass```

Use fact for information about the Class.

For example:

![](img/poly_ex1.PNG)

Should return something like:

![](img/poly_ex1out.PNG)

#### python special functions
Class functions that begin with double underscore __ are called special functions in Python.

These functions are not the typical functions that we define for a class. The __init__() function we defined above is one of them. It gets called every time we create a new object of that class.

There are numerous other special functions in Python. Visit [Python Special Functions](https://docs.python.org/3/reference/datamodel.html#special-method-names) to learn more about them.

Using special functions, we can make our class compatible with built-in functions.

#### Class and static methods
Python includes two built-in functions that are intended for use in decorating methods. The staticmethod() function modifies a method so that the special behavior of providing the instance as an implicit first argument is no longer applied. In fact, the method can be called on either an instance or the class itself, and it will receive only the arguments explicitly provided to the call. It becomes a static method. You can think of static methods as being functions that don't need any information from either their class or their instance, so they do not need a reference to it. Such functions are relatively infrequently seen in the wild.

If you want to write a method that relies on data from the class (class variables are a common way to share data among the various instances of the class) but does not need any data from the specific instance, you should decorate the method with the classmethod() function to create a class method. Like static methods, class methods can be called
on either the class or an instance of the class. The difference is that the calls to a class method do receive an implicit first argument. Unlike a standard method call, though, this first argument is the class that the method was defined on
rather than the instance it was called on. The conventional name for this argument is cls, which makes it more obvious that you are dealing with a class method.

You may well ask what static and class methods are for. Why use them when we already have standard methods that are perfectly satisfactory for most purposes? Why not just use functions instead of static methods, since no additional arguments are provided? The answer to this question lies in the fact that these functions are methods of a class, and so will be inherited (and can be overridden or extended) by any subclasses you may define. Further, the instances of the class can reference class variables rather than using a global. This is always safer because there is no guarantee, when your code lands in someone else's program, that their code isn't using the same global name for some other purpose. It is difficult to think of any example where the use of a classmethod would be absolutely required, but
sometimes it can simplify your design a little.

A typical application for class methods has each of the instances using configuration data that is common to all, and saved in the class. If you provide methods to alter the configuration data (for example, changing the frequency a wireless transmitter works on, or changing the function that the instances call to allocate resources), they do not need to reference any of the instances, so a class method would be ideal.

##### Class methods
A class method receives the class as its first argument.
Define class methods with builtin function classmethod() or with decorator @classmethod.
See the description of classmethod() builtin function at "Builtin Functions": https://docs.python.org/3/library/functions.html#classmethod

##### Static methods
A static method receives neither the instance nor the class as its first argument.
Define static methods with builtin function staticmethod() or with decorator @staticmethod.
See the description of staticmethod() builtin function at "Builtin Functions": https://docs.python.org/3/library/functions.html#staticmethod


## Exercise
Create three classes that contain the following method
1) Shape: __ __init__ __(self,name), __area__(self), fact(self), __ __str__ __(self)
2) Square: __ __init__ __(self,length), __area__(self), fact(self), __ __str__ __(self)
3) Circle: __ __init__ __(self,radius), __area__(self), fact(self), __ __str__ __(self)

Use the ```Shape class``` as a base and let the square/circle inheirit its properties. Use polymorphism to alter the area and __ __str__ __ methods.

Based on your classes the following code

![](img/poly_ex1.PNG)

Should return something like:

![](img/poly_ex1out.PNG)