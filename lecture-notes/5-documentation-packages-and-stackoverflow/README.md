# Chapter 5. Documentation, packages and StackOverflow

In this chapter you will get to know the Python documentation and learn to appreciate and effectively work with documentation in general. [The docs](http://www.writethedocs.org), as they are referred in the software community, describe in full detail what a certain piece of software does, how it should be used and often times provides a rich set of usage examples. High-quality documentation is vital for one's ability to quickly and effectively understand and use any piece of software, be it a programming language, a third-party module or a desktop application.

Consuming documentation comes into play once you start delving deeper into Python and, more importantly, when you set out to extend its capabilities by way of third-party modules. Modules are bundles of code you can inject and use in your code that are written by fellow developers, scientist and domain experts. They allow you tackle complex real-world problems/challenges in the fields of [spatial analysis](https://shapely.readthedocs.io/en/latest/manual.html) and [modelling](https://pysal.org/model/), [managing (big) data](https://www.psycopg.org/docs/), [web applications](https://www.djangoproject.com), [machine learning](https://www.tensorflow.org) and many more.

While most developers do their utmost best to craft high-quality code and documentation you will inevitably run into problems and bugs. Each has a solution: StackOverflow and issue trackers. The former is an online community geared at solving and answering development-related problems and questions. The latter is the de-facto standard for communicating with developers about their code.

# Rationale

Python has a rich, mature and [ever growing](https://stackoverflow.blog/2017/09/06/incredible-growth-python/) ecosystem of domain-specific modules many with their own community of developers and users. Finding the right module for the job and judging its usefulness, quality and level of support/development by its community (or developer) can sometimes be challenging and time consuming.

This chapter will present you with a number of characteristics that will help you pick the right modules.

# Outcomes

- the ability to effectively parse large quantities of documentation
- the ability to find external Python modules/packages
- effectively judge whether they are fit for purpose and are maintained
- the ability to effectively search for answers on StackOverflow

# Resources

## Lecture notes

### Python modules

A Python module is a collection of Python functions and classes written by domain experts that you can install on your machine, `import` and use in your code.

#### Where to find and how to install them?

Generally you can find modules in three places on the web: the [Python Package Index](https://pypi.org/) (PyPI), [conda repositories](https://anaconda.org/anaconda), and GitHub/GitLab.

##### conda

When you are using an Anaconda distribution of Python it is recommended to install modules through the conda installer. To install a module through conda open an Anaconda prompt (on Windows) (`Start Menu` -> `Anaconda` -> `Anaconda Prompt`) or a Terminal (on macOS) and enter the following command:

    conda install <package_name>

You can find if a module is available on conda by googling `conda <package_name>` and look for the result (with `Anaconda Cloud` in the title) with the latest version available for your platform. Copy the text under `To install this package with conda run one of the following:` and enter this command in the Anaconda prompt / Terminal.

You can also use the Anaconda Navigator to search and install packages if you prefer a graphical interface.

##### pip

If a module is not available through conda you can try PyPI packages, which are installed through the Python command line utility `pip` as

    pip install <package_name>

`pip` comes pre-installed on macOS and Linux. On Windows you will need to install Python.

If your package/module of choice is not on PyPI (and thus not installable by `pip`), you will need to read its `Installation` instructions and install the package/module manually.

#### How to use modules?

Once you've found and installed a useful module you are ready to `import` it in your Python scripts like so

```python
import shapely
```

This tells Python to fetch the `shapely` module and make it available for use. Shapely is a Python package for performing geometrical spatial analysis. It can, for instance, calculate the length of a linestring. Consider the following script

```python
from shapely.geometry import LineString

line_vertices = [(0, 0), (2, 0), (3, 1), (4, 3)]
highway = LineString(line_vertices)
print(highway.length)
```

We start by importing `shapely` on line 1. On line 3 we create a list of (x,y) coordinate pairs that signify our linestring. On line 4 we call Shapely's `LineString` function which is located in the `geometry` submodule. `LineString` creates a line which we store in `highway`. On the last line we ask Shapely to compute the length of our line.

The print statement outputs `5.650281539872885`. Voilà!

Shapely can do much more; let's see how to calculate a polygon's area. Off to the Shapely documentation!

### The documentation

A module's documentation is as important as its source code. Without it it will be fairly painful to determine what a module can do and how you should use it. This is contrary to desktop and mobile apps which you can explore by clicking/tapping in a semi-random manner and "seeing what happens".

High-quality and mature documentation consists of the following components:

- introduction - gives a high level overview of the module. It often times describes the need for this particular module, the design decisions, how it compares to similar modules, how to install and use it, who develops it and how to contact them, where (forum, mailing list) to find the community (if it exists) and how to report bugs and contribute code/documentation. It is a map of a module's ecosystem, if you wish.
- user guide/manual - a structured, book-like description of a module meant to be read from start to finish much like an IKEA manual. Its goal is to provide a thorough understanding of a module's functionalities and how they relate. Guides/manuals are fairly wordy and use a lot of code examples and images.
- tutorials/HOWTOs/cookbooks - teach how to perform a specific task or use a certain function/method in a step-by-step manner employing copious amounts of examples/code.
- reference/API - an exhaustive and/but succinct _technical_ description of a module's submodules, objects and functions/methods. Short on words, images and examples, the reference lists a module's functions in alphabetical order and provides their input and output parameters alongside a short description of their effects.

Each type of documentation serves a different purpose. The introduction helps you decide whether this module fulfills your needs and charts its ecosystem. The user guide/manual describes the basic and most important concepts and use cases. The tutorials/cookbooks show you in a step-by-step manner how to combine several functions/methods to achieve more complex tasks and specific use cases. The reference is an exhaustive list of everything a module can do. You will generally refer to it after you have grasped the basics or outgrown the tutorials.

An effective strategy for quickly exploring new modules is to look for example code by scanning the introduction and table of contents of the user guide/manual and navigating to the section you suspect is useful. Browse through the tutorials if you do not immediately find what you are looking for. Look at the reference as a last resort but keep in mind that you probably will not find examples there.

Documentation generally comes as a set of web pages or a PDF file (for offline consumption). As such, it can live anywhere on the web: on a module's web page, GitHub/GitLab or specialised documentation hosting platforms such as [ReadTheDocs](https://readthedocs.org).

#### The Shapely documentation

Let us find out how to calculate the area of a polygon using Shapely.

Googling for `shapely` returns the three most important things you will need when working with modules: [Shapely's source code (on GitHub)](https://github.com/Toblerity/Shapely), its [PyPI entry](https://pypi.org/project/Shapely/) and a link to its documentation.

Navigating to [The Shapely User Manual](https://shapely.readthedocs.io/en/latest/manual.html) lands us on a single page that contains (almost) all there is to know about Shapely. The `Introduction` section explains what Shapely is, why it exists, when it should (not) be used and concludes with a general description of its premises and capabilities.

The `Table of Contents` on the right gives a quick overview of the manual's contents which, of course, is also an overview of Shapely's capabilities. The [Geometric Objects](https://shapely.readthedocs.io/en/latest/manual.html#geometric-objects) section introduces the geometries we can work with i.e. points, lines, polygons, etc. The [General Attributes and Methods](https://shapely.readthedocs.io/en/latest/manual.html#general-attributes-and-methods) section lists the methods and operations these geometries support i.e. length, area, buffer, distance, etc.

Navigating to the [Polygons](https://shapely.readthedocs.io/en/latest/manual.html#polygons) section presents us with a fairly formal definition of the `Polygon` geometry that states

> The Polygon constructor takes two positional parameters. The first is an ordered sequence of `(x, y[, z])` point tuples and is treated exactly as in the _LinearRing_ case. The second is an optional unordered sequence of ring-like sequences specifying the interior boundaries or “holes” of the feature.

Albeit necessary this description is not overly useful as it does not show us exactly how to create a polygon. It would be nice to see an example. Luckily, there is one just past the figures that states

```python
from shapely.geometry import Polygon

polygon = Polygon([(0, 0), (1, 1), (1, 0)])
print(polygon.area)
print(polygon.length)
```

The output is `0.5` and `3.4142135623730949`. And there we are, we calculated the area of a polygon! We will explore Shapely further in the next chapter and in Assignment 2.

### Python's built-in help facilities

Python comes with built-in functions that allow you to explore modules's documentation and capabilities.

The `help()` function prints the documentation of functions and modules in the console. It is available in Python' interactive interpreter. Let us request the `print` function's documentation

- type `help('print')` in the python console

Spyder can also automatically open a function's help. To enable this feature go to the `help` tab of the preferences (Tools -> Preferences -> Help) and enable `Automatic connections` in the editor and console. Spyder will now show the help automatically in the same pane as the variable explorer as soon as you open the parenthesis of a function call.

The `dir()` function lists a Python entity's capabilities. Let us list all the methods supported by a Python dictionary.

- create an empty dictionary in the python console by typing e.g. `cities = {}`
- execute `dir(cities)`. The result is a list of all the operations supported by dictionaries e.g. `keys`, `update`, `values`, etc.

You can execute a method by calling it as e.g. `cities.keys()`.

The python console in Spyder also features autocompletion, which means it will show the available methods of an object as soon as you type the `.`.

- in the python console type `cities.` (note the period)
- press the `Tab` key

### Beyond the docs: online communities

The docs will only get you so far... sooner or later you will run into bugs, unexpected behaviour and undocumented features. StackOverflow and the StackExchange family of communities have you covered.

#### StackOverflow

[StackOverflow](https://stackoverflow.com) (SO) is an online Questions & Answers community geared towards developers (and programmers of all kinds). Its strict format, clear presentation, and voting and user points system have turned it into the go-to online resource for programmers and developers. SO has become so ingrained in people's workflow that everybody jokes about going home (or quitting their jobs) in the rare instances that SO goes down.

StackOverflow has risen to prominence thanks to its strict adherence to the Q&A model. Users are allowed to only post questions that can be answered by facts (i.e. snippets from the documentation) or working code. Anything in between is removed by the administrators. This is especially true for highly subjective questions such as "what is the best programming language/module/editor/keyboard".

Each answer can be up- or downvoted and the original poster can pick one (not necessarily the most popular) as a solution to his/her problem. That answer is then marked with a checkmark and placed under the question. Users receive reputation points on various interactions with the site and other users; popular user are allowed to do more on the site than newcomers.

Over the years SO has amassed a huge amount of high-quality question/answer pairs. You are likely to find answers to almost any of the questions/bugs you come across in your (early) development career by Googling for them in verbatim.

StackOverflow has, being an online community, its downsides as well. There are complaints about overzealous administrators, trolls and a certain hostility towards new users. While the reputation system is meant to weed out spammers and lazy posters it does introduce a barrier to entry. Whether this is good or bad is debatable... SO as a resource is and remains extremely useful and you are encouraged (and probably already have) to use it as often as you need.

#### gis.stackexchange.com

StackOverflow's popularity and success made its creators realize that the strictly curated Q&A format can be applied to non-programming topics and themes as well. They generalized the platform and allowed users to create separate thematic SO-like communities: [StackExchange](https://stackexchange.com).

A StackExchange community exists for [almost every major topic imaginable](https://stackexchange.com/sites).

Of particular interest for geo experts is [gis.stackexchange.com](https://gis.stackexchange.com): a curated Q&A community for everything geo related: [geo Python, QGIS, ArcGIS, GDAL, OpenLayers, etc](https://gis.stackexchange.com/tags).

### Judging usability and longevity

Python's rich ecosystem is a double edged sword: the multitude of modules means you will probably find a third-party module/package to do your bidding. On the other hand, sometimes there will be more than one option to choose from; determining which one to pick can be a challenge.

#### The anatomy of a high-quality module/package

A high-quality module/package has a great documentation and a lively social ecosystem. The former allows you to quickly and effectively get up to speed and include it in your code. The latter ensures that the module will be around for the foreseeable future: up-to-date, bug free and secure.

A badly documented or abandoned module is a liability you should try to avoid as much as possible. Learning how a module works by reading its source code is tough; abandoned modules may cause you tremendous trouble when (rather than if) you encounter a bug or strange behaviour in a corner case as no one (not even StackOverflow) will be able to help you.

GitHub and GitLab, together with a package's documentation, give you an insight into a Python module's (and open source projects in general) quality, and development and support state.

#### The strength of an ecosystem

Building software is, contrary to first and perhaps naive appearances, a highly social activity. Big software projects, especially, are like miniature societies: it is the coming together of people who collaborate and make collective decisions on the basis of self-designed process and protocols for communication. A software construction site (i.e. a public repository) is like a real-life construction site: busy, noisy and beautiful.

GitHub and GitLab expose the buzz of open source projects by publicizing the various stages of the Git-based development process. Take the Shapely repository on GitHub for instance. At the time of writing it had the following properties

1.  the latest commit was 3 days ago
2.  the latest release was 4 months ago
3.  148 open and 671 closed issues
4.  most of the open issues have more than one comment
5.  7 open and 463 closed pull requests
6.  88 people watching the repository
7.  2.6k stars
8.  445 forks
9.  121 contributors

We can distill the following from these numbers

- the project is under active development
- the closed/open issue ratio is high. This means that Shapely's maintainers react to raised issues. Whether they fix the stated problems or not is not really relevant as some issues may be unrealistic or out-of-scope feature requests.
- the maintainers use the issue tracker for design/implementation discussions and not only for registration of problems and feature requests (as those often do not generate much discussion)
- a large number of external developers have contributed code (the core maintainers probably commit to master directly). In other words, Shapely has a lively community
- 88 people get notifications about _all_ the conversations in the repository. In other words, 88 people are interested in everything that happens in this project. A lot of eyes will see your bug report or code contribution!
- 2.6k people have "faved" the repository.
- 445 people have forked the project on GitHub.
- 121 people have contributed code to Shapely. That means 54 people are comfortable with the code and will be able to take over in the highly unlikely event that the core maintainer decides to move on to a new/different project.

In other words, Shapely is a healthy open source project which should be safe to use if it fulfills your needs and you are comfortable with its documentation.

# Exercise (base for TAA3)

Reading and writing spatial data files is the start/end point of many a spatial analysis. As such, several Python modules exist.

Which packages do pop-up when you look through websites like StackOverflow and gis.stackexchange? What do you find when you are searching on google for geospatial python packages?

Search for (at least 3) geospatial Python packages. These packages must be useful to anyone who needs to read-, write- and analyze geospatial data with Python. Make notes of the pros and cons. Use the theory from this chapter to evaluate the packages.
